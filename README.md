# JGI/Proteowizard
##
## Instructions for creating a docker files to push for shifter repository.

Under `Settings > CI/CD > Variables` set the environment variables `SHIFTER_TOKEN` 
and `SHIFTER_USER` to point to the token created when you first login to 
the shifter repository and shifter user to be your username or a username that
everyone can have access to.

## Creating a .gitlab-ci.yml file

This is an example used from the JGI-CI/bbtools repository. Key items here
are to use the docker-in-docker gitlab services for building containers. Then
you would push to the registry as you normally would.

```yaml
variables:
  GIT_STRATEGY: clone
  SHIFTER: registry.services.nersc.gov
  CONTAINER_NAME: proteowizard
  DOCKER_NAME: $SHIFTER_USER/$CONTAINER_NAME

before_script:
  - echo "Starting..."

stages:
  - deploy      # Deploy the software or Docker image for production

deploy:
# You can use gitlab's own docker-in-docker services to create docker images.
  stage: deploy
  image: docker:git
  services:
      - docker:dind
  script:
    - echo "Deploying to registry"
    - docker login $SHIFTER -u $SHIFTER_USER -p $SHIFTER_TOKEN
    - docker tag $SHIFTER/$DOCKER_NAME
    - echo ${SHIFTER}/${NAME}
    - docker push $SHIFTER/$DOCKER_NAME
    - echo "Pushed $SHIFTER/$DOCKER_IMAGE"
```
